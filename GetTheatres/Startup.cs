﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace GetTheatres
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(a => {
                    a.TokenValidationParameters = GetTokenValidationParameters();
                   
                });
          
               
        }
        private static string getSecret()
        {
            return "L+en2sz63vYXYQKpPgjO0Iql+llD8rJgGWpAv38ZUITS1vGDjzJ6X1VLjD7KpKQcGUVFN8QgMS8wX1bjdxpCCA==";
        }
        private static TokenValidationParameters GetTokenValidationParameters()
        {
            string Secret = getSecret();
            byte[] key = Convert.FromBase64String(Secret);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(0),
                ValidateLifetime = true,
                RequireExpirationTime=true,
                ValidateAudience = false,
                ValidateIssuer = false,
               // ValidIssuer = "test",
                //ValidAudience = "testaad",
                IssuerSigningKey = securityKey,
                ValidateIssuerSigningKey = true,
                RequireSignedTokens = true,
                ValidateActor = false


            };
            return tokenValidationParameters;
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
               // app.UseHsts();
            }

 app.UseCors(policy => 
            policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
               );
            //app.UseHttpsRedirection();
            app.UseMvc();
            app.UseAuthentication();
        }
    }
}
