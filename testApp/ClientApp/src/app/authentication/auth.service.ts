import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http'


@Injectable()
export class AuthService {
  base_url: string;
  token: string;
  http:HttpClient;
  constructor(@Inject('BASE_URL') baseurl: string,http:HttpClient) {

    this.base_url = baseurl;
    this.http=http;
  }
  

  public getToken(): string {
    console.log(localStorage.getItem('token'));
    return localStorage.getItem('token');
  }

  public isAuthenticated(): boolean {
    const helper = new JwtHelperService();
    const token = this.getToken();
    const decodedToken = helper.decodeToken(token);
    return helper.isTokenExpired(token);
  }

  public getLogin(){
return this.http.get(this.base_url+'api/Auth/Login').subscribe();
    
  }


  public SetToken(): boolean {
    var output: boolean;
    $.ajax({
      type: "GET",
      contentType: "application/json; charset=utf-8",
      url: 'https://localhost:44327/api/Auth/GetAuth',

      cache: false,
      success: (result) => {

        localStorage.setItem('token', result);
        output = true;
      },
      error: function (error) {
        console.log(error);
        output = false;
      }
    });

    
    return output;
  }

}


