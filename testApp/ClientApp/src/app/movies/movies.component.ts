import { Component, OnInit } from '@angular/core';
import {GetMoviesService} from '../get-movies.service'
import {Movie} from '../models/Movie'
import {UploadtoBlobService} from '../uploadto-blob.service';
import { FormGroup, FormControl, Validators, FormBuilder, Form } from '@angular/forms';
import {SaveMoviesService} from '../save-movies.service';
import {UpdateMovieService} from '../update-movie.service';
import {DeleteMovieService} from '../delete-movie-service.service'
import { templateJitUrl } from '../../../node_modules/@angular/compiler';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
  providers:[GetMoviesService,UploadtoBlobService,SaveMoviesService,UpdateMovieService,DeleteMovieService]
})



export class MoviesComponent implements OnInit {
  editFlag:boolean=false;
  formData:FormData;
  movieForm:FormGroup;
  editMovie:Movie;
  actorName:string;
  fileUpload:any;
  validationMessages:any;
  posterUrl: string;
  private movies:Movie[];
  private addFlag:boolean=false;
  constructor(private getMoviesService:GetMoviesService,fb:FormBuilder, private uploadService:UploadtoBlobService,private saveMovies:SaveMoviesService,private deleteMovieService:DeleteMovieService) { 

    // ,Validators.pattern('/^((19|20)[0-9]{2})$/')]
this.movieForm=fb.group(
  {"MovieName": new FormControl('',Validators.required),
"Plot": new FormControl('',Validators.required),
"Year": new FormControl('',Validators.required),
"Actorslist":new FormControl('',Validators.required),
"Poster": new FormControl('',Validators.required),
"ProducerName":new FormControl('',Validators.required)
  });
}

  ngOnInit() {
    this.editFlag=false;
     this.getMoviesService.getMovies().subscribe(x => {
    
      this.movies = x as Movie[];
      console.log(this.movies); 
    
    });

console.log(this.movies);
      this.validationMessages = {
        MovieName:[{ type: 'required', message: ' is required'}] ,
        Plot:[{ type: 'required', message: ' is required' }],
        Year:[{type:'pattern', message:' invalid format, should valid Year'},
        {type: 'required', message: ' is required' }]  ,
        ActorsList:[ { type: 'required', message: ' is required'}],
        Poster:[{  type: 'required', message: ' is required' }],
        ProducerName:[{  type: 'required', message: ' is required' }]
       
        
      };
    }

 onSubmit(movie:Movie)
 {
  movie.Poster=this.posterUrl;
   console.log(movie);
   debugger;
  movie.Actorslist=movie.Actorslist.toString().split(",");
   console.log(movie);
   console.log(movie.Actorslist);
   console.log(this.actorName);
   
   this.saveMovies.saveMovie(movie).subscribe(a=>
    {console.log(a);
      this.addFlag=false;
      this.ngOnInit();
    });

 }

 handlefiles(files:FileList)
 {
this.fileUpload=files.item(0);


// this.formData.append('poster', files[0], files[0].name);
this.uploadService.uploadFile(this.fileUpload).subscribe(poster=>{
  console.log(poster);
  this.posterUrl=poster;});


 }

 emitOutput(event:Movie)
 {
this.editFlag=true;
this.editMovie=event;
this.editFlag=true;
 }
 ReloadPage()
 {
   this.ngOnInit();
 }

 delete(event:Movie)
 {
console.log(event);


this.deleteMovieService.RemoveMovie(event.MovieName).subscribe(y=>{console.log(y);
if(y==true)
{
  this.ngOnInit();
}
});


 }


}

