import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Movie } from '../models/Movie';
import { NgForm ,FormsModule,ReactiveFormsModule, Form,FormGroup,FormControl,Validators} from '@angular/forms';
import { UpdateMovieService } from '../update-movie.service';
import { UploadtoBlobService } from '../uploadto-blob.service';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {
  @Input() movie: Movie;
  fileUpload: File;
  posterUrl: string;
  editForm: FormGroup;
validationMessages:any[]
@Output() moviesHome: EventEmitter<any> = new EventEmitter<any>();
  constructor(private updateMovieService: UpdateMovieService, private uploadService:UploadtoBlobService) { 

  }

  ngOnInit() {

    this.validationMessages = [
      {  type: 'required', message: ' is required' }
      
    ];
    this.editForm = new FormGroup(
      {
        "MovieName": new FormControl('', Validators.required),
        "Plot": new FormControl('', Validators.required),
        "Year": new FormControl('', Validators.required),
        "ActorsList": new FormControl('', Validators.required),
        "Poster": new FormControl(),
        "ProducerName": new FormControl('', Validators.required)
      });
   
  }

  
  NavigateToMovies() {
    this.moviesHome.emit();
}


  handlefiles(files: FileList) {
    this.fileUpload = files.item(0);


    // this.formData.append('poster', files[0], files[0].name);
    this.uploadService.uploadFile(this.fileUpload).subscribe(poster => {
      console.log(poster);
      this.movie.Poster = poster;
    });


  }

UpdateMovie()
{
  debugger;
console.log(this.movie);
this.updateMovieService.updateMovie(this.movie).subscribe(x=>{
console.log(x);
if(x==true)
{this.NavigateToMovies();}
}
  );
}

}
