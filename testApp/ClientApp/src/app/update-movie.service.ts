import { Injectable,Inject } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { Observable } from "rxjs";
import {Movie} from './models/Movie'
const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) } 

@Injectable()
export class UpdateMovieService {
  private baseurl:string;
  constructor(@Inject ('BASE_URL') baseurl:string,private httpclient:HttpClient) { this.baseurl=baseurl;}

  updateMovie(movie:Movie):Observable<boolean>
  {
   
const body={
'MovieName':movie.MovieName,
'Plot':movie.Plot,
'Year':movie.Year,
'Poster':movie.Poster,
'Actorslist':movie.Actorslist,
'ProducerName':movie.ProducerName
}
console.log(this.baseurl);
return this.httpclient.post<boolean>(this.baseurl+'api/Movies/EditMovie',JSON.stringify(body),httpoptions);
  }
}
