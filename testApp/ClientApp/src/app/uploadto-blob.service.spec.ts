import { TestBed, inject } from '@angular/core/testing';

import { UploadtoBlobService } from './uploadto-blob.service';

describe('UploadtoBlobService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadtoBlobService]
    });
  });

  it('should be created', inject([UploadtoBlobService], (service: UploadtoBlobService) => {
    expect(service).toBeTruthy();
  }));
});
