import { Injectable, Inject } from '@angular/core';
import { Observable } from '../../node_modules/rxjs/Observable';

import {HttpClient,HttpHeaders} from '@angular/common/http'

const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }),responseType:'text' } 

@Injectable()
export class UploadtoBlobService {
formData:FormData;
baseurl:string;
  constructor(private http:HttpClient,@Inject ('BASE_URL') baseurl:string) { 

    this.baseurl=baseurl;
  }

  uploadFile(file:File):Observable<string>
  {
debugger;
    let formData: FormData = new FormData();
    formData.append('Document', file);
    //formData.append('groupId', groupId);
    formData.append('Name', file.name);

    return this.http.post(this.baseurl+'api/Blob/Upload', formData, {responseType:'text'});
  }
}
