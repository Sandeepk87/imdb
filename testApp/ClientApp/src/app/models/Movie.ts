export class Movie {
  MovieName: string = '';
  Plot?: string = '';
  Year: string = '';
  Actorslist: string[] = [];
  Poster?: string = '';
  ProducerName: string = '';
}