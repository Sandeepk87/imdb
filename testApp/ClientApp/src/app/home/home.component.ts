import { Component,OnInit } from '@angular/core';
import { GetTheatresService} from '../get-theatres.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  private theatres:string;
  constructor(public getTheater:GetTheatresService)
  {

  }
  ngOnInit()
  {
this.getTheater.getTheatres().subscribe(a=>
  {
this.theatres=a;

console.log("this is get theater output:"+a);

  });
 


  }
}
