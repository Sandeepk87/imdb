import { Injectable,Inject } from '@angular/core';
import { Movie } from './models/Movie';

import { HttpClient,HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";


const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) } 

@Injectable()
export class DeleteMovieService {
baseurl:string
  constructor(private httpclient:HttpClient,@Inject('BASE_URL') base_url:string) { 
this.baseurl=base_url;

  }

  RemoveMovie(movieName:string):Observable<any>
  {
    debugger;
    // const url = `${this.baseurl}/${id}`;
console.log(movieName);
return this.httpclient.delete(this.baseurl+'api/Movies/DeleteMovie/'+`${movieName}`);

  }

}
