import { TestBed, inject } from '@angular/core/testing';

import { SaveMoviesService } from './save-movies.service';

describe('SaveMoviesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaveMoviesService]
    });
  });

  it('should be created', inject([SaveMoviesService], (service: SaveMoviesService) => {
    expect(service).toBeTruthy();
  }));
});
