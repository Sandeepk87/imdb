import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {Inject} from '@angular/core'
import { HttpClient } from '@angular/common/http';
import {AuthService} from '../authentication/auth.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[AuthService]
})
export class LoginComponent implements OnInit {
  private router: Router;
  private baseurl: string;
  private httpclient: HttpClient;
  private authService:AuthService;
  constructor( router:Router,@Inject ('BASE_URL') base_url:string,http:HttpClient,authService:AuthService) {
    this.authService=authService; 

    this.router = router;
    this.baseurl = base_url;
    this.httpclient = http;
  }

  ngOnInit() {
  }

Login($event)
{debugger;
 // window.location.href = this.baseurl + '/api/Auth/Login';  
  document.location.href = this.baseurl + 'api/Auth/Login';;
  //this.httpclient.get(this.baseurl + 'api/Auth/Login');
//this.authService.getLogin();
}

}
