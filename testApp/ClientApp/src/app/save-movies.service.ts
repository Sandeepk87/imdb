import { Injectable, Inject } from '@angular/core';

import { HttpClient ,HttpHeaders} from "@angular/common/http";
import { Observable } from "rxjs";
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { Movie } from './models/Movie';
import { moveEmbeddedView } from '@angular/core/src/view';
import { getBaseUrl } from '../main';

const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) } 



@Injectable()
export class SaveMoviesService {
private baseurl:string;
  constructor(@Inject ('BASE_URL') baseurl:string,private httpclient:HttpClient) { this.baseurl=baseurl;}

  saveMovie(movie:Movie):Observable<boolean>
  {
   
const body={
'MovieName':movie.MovieName,
'Plot':movie.Plot,
'Year':movie.Year,
'Poster':movie.Poster,
'Actorslist':movie.Actorslist,
'ProducerName':movie.ProducerName
}
console.log(this.baseurl);
return this.httpclient.post<boolean>(this.baseurl+'api/Movies/AddMovies',JSON.stringify(body),httpoptions);
  }

}
