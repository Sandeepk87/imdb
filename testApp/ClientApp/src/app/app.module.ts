import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,NgForm } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';

import { MoviesComponent } from './movies/movies.component';
import { GetMoviesService } from './get-movies.service';
import { ReactiveFormsModule } from '@angular/forms';
import {SaveMoviesService} from './save-movies.service'
import { UploadtoBlobService } from './uploadto-blob.service';
import { EditMovieComponent } from './movies/edit-movie.component';
import { UpdateMovieService } from './update-movie.service';
import { DeleteMovieService } from './delete-movie-service.service';

import { GetTheatresService } from './get-theatres.service';

import { AuthInterceptor } from './interceptor/authinterceptor.service'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthService } from './authentication/auth.service';
import { LoginComponent } from './login/login.component'
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
   
    MoviesComponent,
    EditMovieComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
       RouterModule.forRoot([
      { path: '', component: LoginComponent, pathMatch: 'full' },
      { path: 'home', component: HomeComponent},
    
      {path:'movies',component:MoviesComponent},
      
      
    ]),
  
  ],
  providers: [GetMoviesService,SaveMoviesService,UploadtoBlobService,UpdateMovieService,DeleteMovieService,GetTheatresService,AuthService,{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
