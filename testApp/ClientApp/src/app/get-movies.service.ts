import { Injectable, Inject } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Movie} from './models/Movie';
const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) } 



@Injectable()
export class GetMoviesService {
  private Movies: Movie[];
  private _httpclient: HttpClient;
  private _baseurl: string;
  constructor(httpclient: HttpClient, @Inject ('BASE_URL') baseurl:string) {
    this._httpclient = httpclient;
   
    this._baseurl = baseurl;
  }
  getMovies(): Observable<any> {
   
    return this._httpclient.get<any>(this._baseurl+'/api/Movies/GetMovies');
  }
}




