import { Injectable ,Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


const httpoptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }


@Injectable()
export class GetTheatresService {
  private _httpclient: HttpClient;
  private _baseurl: string;
  constructor(httpclient: HttpClient, @Inject('BASE_URL') baseurl: string) {
    this._httpclient = httpclient;

  }
  getTheatres(): Observable<any> {
    debugger;
    return this._httpclient.get("https://localhost:44311" + "/api/Theatres",{responseType:'text'});
  }
}
