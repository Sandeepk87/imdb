﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;

namespace testApp.Controllers
{
    
    public class HomeController : Controller
    {
        private IHttpContextAccessor _contextAccessor;
        public HomeController(IHttpContextAccessor httpContextAccessor)
        {
            _contextAccessor = httpContextAccessor;
        }

        [Route("Index")]
        public IActionResult Index()
        {
            var bodyout = readBodyAsync(_contextAccessor).Result;
            var token = bodyout.Split("=").Last();
            var handler = new JwtSecurityTokenHandler();
            var decoded = handler.ReadJwtToken(token);
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            string stsDiscoveryEndpoint = "https://imdbapp.b2clogin.com/imdbapp.onmicrosoft.com/v2.0/.well-known/openid-configuration?p=B2C_1_SigninSignup";
            var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(stsDiscoveryEndpoint, new OpenIdConnectConfigurationRetriever(), new HttpDocumentRetriever());
            var discoveryDocument = configurationManager.GetConfigurationAsync().Result;


            var _issuer = discoveryDocument.Issuer;
            var signingKeys = discoveryDocument.SigningKeys;
            var keyurl = "https://login.microsoftonline.com/te/imdbapp.onmicrosoft.com/B2C_1_SigninSignup/discovery/v2.0/keys";
            var validationParameters = new TokenValidationParameters
            {
                 IssuerSigningKeys = signingKeys,
                RequireSignedTokens = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidAudience = "2c26fec1-ea0d-41a5-a7ea-37c15fc8ce37",
                 ValidIssuer = _issuer,
            };
            try
            {
                SecurityToken validatedToken = new JwtSecurityToken();
            var current=    tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
                System.Threading.Thread.CurrentPrincipal = current;
                if(_contextAccessor.HttpContext!=null)
                _contextAccessor.HttpContext.User = current;

            }
            catch(Exception e)
            {
                var s = e;
                return Unauthorized();
            }
            return Redirect("/home");
        }

        private static async Task<string> readBodyAsync(IHttpContextAccessor httpContextAccessor)

        {

            System.IO.StreamReader r = new System.IO.StreamReader(httpContextAccessor.HttpContext.Request.Body);

            return await r.ReadToEndAsync();

        }

    }
}