﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace testApp.Controllers
{
   // [Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme+","+CookieAuthenticationDefaults.AuthenticationScheme)]
    
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController: ControllerBase
    {
        public AuthController()
        {

        }
      



        [AllowAnonymous]
            [HttpPost]
        public IActionResult  LoginUser(User user)
        {
            if ((user.UserName.Equals("Sandeep")) && (user.Password.Equals("Test123")))
                return new JsonResult(GetToken());

            else
                return new UnauthorizedResult();
        }


        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAuth()
        {
            return Ok(GetToken());
        }


        [AllowAnonymous]
  
        [ActionName("Login")]
        public IActionResult Login()
        {
            return Challenge(new AuthenticationProperties() { RedirectUri="/home"},AzureADB2CDefaults.OpenIdScheme);
        }

        public IActionResult Logout()
        {

            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var scheme = User.FindFirst("tfp").Value;
            HttpContext.SignOutAsync(scheme);

            return RedirectToAction("Login");
        }
        private static string getSecret()
        {
            return "L+en2sz63vYXYQKpPgjO0Iql+llD8rJgGWpAv38ZUITS1vGDjzJ6X1VLjD7KpKQcGUVFN8QgMS8wX1bjdxpCCA==";

        }
        
        
        private static string GetToken()
        {
            string username = "Sandeepk87";
            string Secret = getSecret();
            byte[] key = Convert.FromBase64String(Secret);
              SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
              SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
              { 
                  Subject = new ClaimsIdentity(new[] {
                      new Claim(ClaimTypes.Name, username),
                  new Claim(ClaimTypes.Expiration,"1") }),
                  
                  Expires = DateTime.UtcNow.AddMinutes(1),
                  SigningCredentials = new SigningCredentials(securityKey,
                  SecurityAlgorithms.HmacSha256Signature)
              };
  	     
  	     JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
              JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
              return handler.WriteToken(token);
        }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}