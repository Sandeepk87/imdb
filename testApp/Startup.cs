using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using DataProvider.Helpers;
using DataProvider;

using System.Linq;
using Microsoft.WindowsAzure.Storage;

using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Threading.Tasks;

namespace testApp
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddLogging();
            services.AddCors();
           
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddHttpContextAccessor();

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });


            services.AddAuthentication((sharedOptions =>
            {
                sharedOptions.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                sharedOptions.DefaultChallengeScheme = AzureADB2CDefaults.AuthenticationScheme;
            }))
             .AddAzureADB2C(options =>

             {
                 options.ClientId = "2c26fec1-ea0d-41a5-a7ea-37c15fc8ce37";

                 options.ClientSecret = "H6w9noMO[0Q#2#kG%MK>z[Ob";
                 options.Instance = "https://imdbapp.b2clogin.com/";
                 options.CallbackPath = "/Index";
                 options.Domain = "imdbapp.onmicrosoft.com";
                 options.SignUpSignInPolicyId = "B2C_1_SigninSignup";
                 options.SignedOutCallbackPath = "/login";



                 //"ClientId": "64f31f76-2750-49e4-aab9-f5de105b5172",
                 //"CallbackPath": "/signin-oidc",
                 //"Domain": "jacalvarb2c.onmicrosoft.com",
                 //"SignUpSignInPolicyId": "B2C_1_SiUpIn",
                 //"ResetPasswordPolicyId": "B2C_1_SSPR",
                 //"EditProfilePolicyId": "B2C_1_SiPe"

             })
             .AddCookie();
           
            
        }



        public static Task OnRedirectToIdentityProvider(RedirectContext context)
        {
            var defaultPolicy = "B2C_1_SigninSignup";
             context.ProtocolMessage.Scope = OpenIdConnectScope.OpenIdProfile;
                context.ProtocolMessage.ResponseType = OpenIdConnectResponseType.IdToken;
               // context.ProtocolMessage.IssuerAddress = context.ProtocolMessage.IssuerAddress.ToLower().Replace(defaultPolicy.ToLower(), policy.ToLower());
                
            return Task.FromResult(0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            app.UseCors(policy => 
            policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
               );

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            })
            ;
            
           

            app.UseSpa(spa =>
            {
               
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";
                //spa.UseAngularCliServer(npmScript: "start");

                if (env.IsDevelopment())
                {
                    spa.Options.StartupTimeout = new TimeSpan(0, 0, 80);
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                   // spa.UseAngularCliServer(npmScript: "start");

                }
                else
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            app.UseAuthentication();
        }


    }
}
