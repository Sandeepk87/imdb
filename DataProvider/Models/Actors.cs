﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataProvider.Models
{
  public  class Actors
    {
        //public Actors()
        //{
        //    Movies = new List<Movies>();
        //}

        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int ActorID { get; set; }
        [Key]
        [Required]
        [Column("Name")]
        public string ActorName { get; set; }


        public Gender Sex { get; set; }


        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Of Birth")]
        [Column("DOB")]
        public DateTime DateOfBirth { get; set; }
        public string Bio { get; set; }
        [ForeignKey("ActorName")]
        public ICollection<ActorsAndMovies> ActorsAndMovies { get; set; } = new HashSet<ActorsAndMovies>();
    }
}
