﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
namespace DataProvider.Models
{
    public class Movies
    {
        
        //public Movies()
        //{
        //    Actors = new List<Actors>();

        //}
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int MovieID { get; set; }

        [Required]
        [Key]
        [StringLength(100, ErrorMessage = "Name cannot exceed 50 characters")]
        [Column("Name")]
        public string MovieName { get; set; }


        [Display(Name = "Year Of Release")]
        [Column(TypeName = "int")]
        [RegularExpression(@"^(19|20)\d{2}$")]

        public string Year { get; set; }

        public string Poster { get; set; }
        public string Plot { get; set; }

        [ForeignKey("MovieName")]
        public ICollection<ActorsAndMovies> ActorsAndMovies { get; set; } = new HashSet<ActorsAndMovies>();

        //[ForeignKey("ProducerID")]
        
        public Producers Producer { get; set; }

    }


}
