﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataProvider.Models
{
 public    class ActorsAndMovies
    {

        public  string MovieName { get; set; }

        public  string ActorName { get; set; }

        public virtual Movies Movie { get; set; }

        public virtual Actors Actors { get; set; }
    }
}
