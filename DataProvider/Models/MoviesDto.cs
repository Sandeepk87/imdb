﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataProvider.Models
{
   public  class MoviesDto
    {
        public string MovieName { get; set;}
        public string ProducerName { get; set; }
        public string Year { get; set; }

        public string Poster { get; set; }
        public string Plot { get; set; }

        public List<string> ActorNames { get; set; }

    }
}
