﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataProvider.Models
{
   public class Producers
    {


        [Required]
        [Key]
        [Column("Name")]
        public string ProducerName { get; set; }


        public Gender Sex { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Column("DOB")]
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
        public string Bio { get; set; }


        public HashSet<Movies> Movies { get; set; } = new HashSet<Movies>();




    }
}
