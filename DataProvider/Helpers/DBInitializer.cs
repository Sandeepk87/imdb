﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataProvider.Models;
namespace DataProvider.Helpers
{
    public class DBInitializer
    {
        public static void Seed(DB_Context context)
        {
            var movie1 = new Movies
            {

                Year = "2000",
                MovieName = "Titanic",
                Plot = "Seventeen-year-old Rose hails from an aristocratic family and is set to be married. When she boards the Titanic, she meets Jack Dawson, an artist, and falls in love with him",
                Producer = new Producers
                {
                },
                ActorsAndMovies = new HashSet<ActorsAndMovies>() { },// new ActorsAndMovies { ActorName="Dicaprio",MovieName="Titanic"} },
                Poster = @"https://csg09788aa7faf6x4ad0x9fc.blob.core.windows.net/imdbtest/titanic.jpg"
            };
            var movie2 = new Movies
            {

                Year = "2010",
                MovieName = "Inception",
                Plot = "Cobb steals information from his targets by entering their dreams. He is wanted for his alleged role in his wife's murder and his only chance at redemption is to perform the impossible, an inception.",
                Producer = new Producers
                {
                },
                ActorsAndMovies = new HashSet<ActorsAndMovies>() { },
                //  ActorsAndMovies = new HashSet<ActorsAndMovies> { new ActorsAndMovies { ActorName="Dicaprio",MovieName="Inception"} },
                Poster = @"https://csg09788aa7faf6x4ad0x9fc.blob.core.windows.net/imdbtest/inception.jpg"
            };




            var actor1 = new Actors
            {

                Bio = "sdfsdfsf",
                ActorName = "Dicaprio",
                DateOfBirth = DateTime.ParseExact("31/12/1979", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                Sex = Gender.Male,
                ActorsAndMovies = new HashSet<ActorsAndMovies>()
                //    MoviesActed = new HashSet<ActorsAndMovies> {
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Titanic"},
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Inception"}
                //         }
                //};
            };
            var actor2 = new Actors
            {

                Bio = "Actress.. born in california",
                ActorName = "Kate Winslet",
                DateOfBirth = DateTime.ParseExact("31/12/1982", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                Sex = Gender.Female,
                ActorsAndMovies = new HashSet<ActorsAndMovies>()
                //    MoviesActed = new HashSet<ActorsAndMovies> {
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Titanic"},
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Inception"}
                //         }
                //};
            };
            var actor3 = new Actors
            {

                Bio = "Brought up in Texas..",
                ActorName = "Ellen Page",
                DateOfBirth = DateTime.ParseExact("31/12/1983", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                Sex = Gender.Female,
                ActorsAndMovies = new HashSet<ActorsAndMovies>()
                //    MoviesActed = new HashSet<ActorsAndMovies> {
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Titanic"},
                //        new ActorsAndMovies{ActorName="Dicaprio",MovieName="Inception"}
                //         }
                //};
            };





            var producer = new Producers
            {

                ProducerName = "Warner Brothers",
                DateOfBirth = DateTime.ParseExact("23/02/1965", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                Bio = "sdfsfsf",
                Movies = new HashSet<Movies>() {
                    new Movies{MovieName="Titanic"},
                    new Movies {MovieName="Inception"}
                },
                Sex = Gender.Male

            };

            
            var actormovie1 = new ActorsAndMovies { ActorName = "Dicaprio", MovieName = "Titanic" };
            var actormovie2 = new ActorsAndMovies { ActorName = "Kate Winslet", MovieName = "Titanic" };


            var actormovie3 = new ActorsAndMovies { ActorName = "Dicaprio", MovieName = "Inception" };
            var actormovie4 = new ActorsAndMovies { ActorName = "Ellen Page", MovieName = "Inception" };




            movie1.Producer = producer;
            movie2.Producer = producer;
            movie1.ActorsAndMovies.Add(actormovie1);
            movie1.ActorsAndMovies.Add(actormovie2);
            movie2.ActorsAndMovies.Add(actormovie3);
            movie2.ActorsAndMovies.Add(actormovie4);
            var moviesList = new HashSet<Movies>();
            moviesList.Add(movie1);
            moviesList.Add(movie2);
            producer.Movies = moviesList;
            var actorsList = new HashSet<Actors>();
            actor1.ActorsAndMovies.Add(actormovie3);
            actor1.ActorsAndMovies.Add(actormovie1);
            actor2.ActorsAndMovies.Add(actormovie2);
            actor3.ActorsAndMovies.Add(actormovie4);
            actorsList.Add(actor1);
            actorsList.Add(actor2);
            actorsList.Add(actor3);


            if (!context.actors.AnyAsync().Result)
            {

                context.actors.AddRange(actorsList);
                //      context.SaveChanges();
            }

            if (!context.movies.AnyAsync().Result)
            {
                context.movies.AddRange(moviesList);
                //    context.SaveChanges();


            };

            


            if (!context.producers.AnyAsync().Result)
            {
                context.producers.Add(producer);
                //        context.SaveChanges();
            }
            context.SaveChanges();
        }


    }
}
