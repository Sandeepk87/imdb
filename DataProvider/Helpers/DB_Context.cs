﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

using DataProvider.Models;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;


namespace DataProvider.Helpers
{
    public class DB_Context : DbContext
    {
        //configure logging for sql commands (filter to include only commands)
        public static readonly LoggerFactory consoleLoggerFactory = new LoggerFactory(new[]
        {
         new ConsoleLoggerProvider((category,level)=>
         category==DbLoggerCategory.Database.Name &&level==LogLevel.Information,true)

        });
        public DB_Context(DbContextOptions<DB_Context> options) : base(options)
        {
        }

        public DbSet<Movies> movies { get; set; }
        public DbSet<Actors> actors { get; set; }

        public DbSet<Producers> producers { get; set; }


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{

        //   // optionsBuilder.UseSqlServer(@"Data Source=SANPC\\SQLEXPRESS;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        //}


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var converter = new ValueConverter<string, int>(
    v => Convert.ToInt32(v),
    v => v.ToString());

            modelBuilder
                .Entity<Movies>()
                .Property(e => e.Year)
                .HasConversion(converter);



            modelBuilder.Entity<Actors>(actor =>
            {
                actor.Property(b => b.DateOfBirth).HasColumnType("Date");
                actor.Property(b => b.Sex).HasConversion<string>();


            });

            modelBuilder.Entity<Producers>(producer =>
            {
                producer.Property(b => b.DateOfBirth).HasColumnType("Date");
                producer.Property(b => b.Sex).HasConversion<string>();
                producer.HasMany(b => b.Movies).WithOne(e => e.Producer)
                .OnDelete(DeleteBehavior.Cascade);

            });



            modelBuilder.Entity<ActorsAndMovies>(ab =>
            {
                ab.HasKey(s => new { s.ActorName, s.MovieName });

                ab.HasOne(s => s.Actors)
                .WithMany(t => t.ActorsAndMovies)
                .HasForeignKey(s => s.ActorName);

                ab.HasOne(s => s.Movie)
                .WithMany(r => r.ActorsAndMovies)
                .HasForeignKey(s => s.MovieName);

            });

        }
    }

}
