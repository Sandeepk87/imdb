﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataProvider.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "actors",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Sex = table.Column<string>(nullable: false),
                    DOB = table.Column<DateTime>(type: "Date", nullable: false),
                    Bio = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_actors", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "producers",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Sex = table.Column<string>(nullable: false),
                    DOB = table.Column<DateTime>(type: "Date", nullable: false),
                    Bio = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_producers", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "movies",
                columns: table => new
                {
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Year = table.Column<int>(type: "int", nullable: true),
                    Poster = table.Column<string>(nullable: true),
                    Plot = table.Column<string>(nullable: true),
                    ProducerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movies", x => x.Name);
                    table.ForeignKey(
                        name: "FK_movies_producers_ProducerName",
                        column: x => x.ProducerName,
                        principalTable: "producers",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActorsAndMovies",
                columns: table => new
                {
                    MovieName = table.Column<string>(nullable: false),
                    ActorName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActorsAndMovies", x => new { x.ActorName, x.MovieName });
                    table.ForeignKey(
                        name: "FK_ActorsAndMovies_actors_ActorName",
                        column: x => x.ActorName,
                        principalTable: "actors",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActorsAndMovies_movies_MovieName",
                        column: x => x.MovieName,
                        principalTable: "movies",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActorsAndMovies_MovieName",
                table: "ActorsAndMovies",
                column: "MovieName");

            migrationBuilder.CreateIndex(
                name: "IX_movies_ProducerName",
                table: "movies",
                column: "ProducerName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActorsAndMovies");

            migrationBuilder.DropTable(
                name: "actors");

            migrationBuilder.DropTable(
                name: "movies");

            migrationBuilder.DropTable(
                name: "producers");
        }
    }
}
