﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetMovies.BusinessLogic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DataProvider.Helpers;
using DataProvider.Models;
using Microsoft.EntityFrameworkCore;
using GetMovies.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;

namespace GetMovies
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddDbContext<DB_Context>(a => a.UseSqlServer(Configuration.GetConnectionString("IMDBTest")));//IMDBDev
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddTransient<IbusinessLogic, businessLogic>();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DataProvider.Models.Movies, Models.MovieView>()
                .ForMember(a => a.Actorslist, c => c.MapFrom(a => a.ActorsAndMovies.Select(b => b.ActorName).ToList()))
                .ForMember(a => a.ProducerName, c => c.MapFrom(a => a.Producer.ProducerName));

                cfg.CreateMap<Models.MovieView, DataProvider.Models.Movies>();
            });
            services.AddCors();
            services.AddTransient<CloudStorageAccount>(s => CloudStorageAccount.Parse(Configuration["BlobSettings:ConnectionString"]));
            services.AddTransient<IBlobService, BlobService>();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(a =>
               {
                   a.TokenValidationParameters = GetTokenValidationParameters();

               });
        }

        private static string getSecret()
        {
            return "L+en2sz63vYXYQKpPgjO0Iql+llD8rJgGWpAv38ZUITS1vGDjzJ6X1VLjD7KpKQcGUVFN8QgMS8wX1bjdxpCCA==";
        }
        private static TokenValidationParameters GetTokenValidationParameters()
        {
            string Secret = getSecret();
            byte[] key = Convert.FromBase64String(Secret);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(0),
                ValidateLifetime = true,
                RequireExpirationTime = true,
                ValidateAudience = false,
                ValidateIssuer = false,
                // ValidIssuer = "test",
                //ValidAudience = "testaad",
                IssuerSigningKey = securityKey,
                ValidateIssuerSigningKey = true,
                RequireSignedTokens = true,
                ValidateActor = false


            };
            return tokenValidationParameters;
        }

        /*
        private static TokenValidationParameters GetTokenValidationParameters()
        {
            var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>("https://login.microsoftonline.com/d7766073-ee5c-4df2-a8d4-49206d8ebeaf/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever(), new HttpDocumentRetriever());
            var discoveryDocument = configurationManager.GetConfigurationAsync().Result;
           

            var _issuer = discoveryDocument.Issuer;
            var signingKeys = discoveryDocument.SigningKeys;
            return new TokenValidationParameters()
            {
                IssuerSigningKeys = signingKeys,
                RequireSignedTokens = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidateAudience = false,
                ValidateIssuer = true,
                ValidIssuer = _issuer
            };
        }
        */

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(policy =>
            policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
               );

         //   app.UseHttpsRedirection();
            app.UseMvc();
            app.UseAuthentication();

        }
    }
}
