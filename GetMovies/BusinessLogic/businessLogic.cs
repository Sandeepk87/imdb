﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetMovies.Models;
using DataProvider;
using AutoMapper.Mappers;
using Microsoft.EntityFrameworkCore;
using DataProvider.Helpers;
using AutoMapper.QueryableExtensions;
using DataProvider.Models;
using GetMovies.Helpers;
using System.Web;

namespace GetMovies.BusinessLogic
{
    public class businessLogic:IbusinessLogic
    {
        private IBlobService _blobService;

        private DB_Context _dbContext;
        public businessLogic(DB_Context dbContext,IBlobService blobService)
        {
            _dbContext = dbContext;
            _blobService = blobService;
        }

        public bool CheckMovieExists(MovieView movie)
        {
            
            return _dbContext.movies.Find(movie.MovieName)  != null ? true : false;
                
        }

      public ICollection<MovieView> GetMovieList() {

            var movielist = _dbContext.movies
                .ProjectTo<MovieView>().ToList();
                
               
            
            //foreach (var movie in movielist)
            //{
            //    MovieView movieView = new MovieView()
            //    {
            //        MovieName = movie.MovieName,
            //        Plot = movie.Plot,
            //        Poster = movie.Poster,
            //        ProducerName = movie.Producer.ProducerName.ToString(),
            //        Year = movie.Year,
            //       // Actorslist = AutoMapper.


            //    };

            //}

            
            return movielist;
        }

        public bool Save(MovieView movie)
        {
            try
            {


                ICollection<ActorsAndMovies> act = new HashSet<ActorsAndMovies>();
                ICollection<Actors> actors = new HashSet<Actors>();
                foreach (var name in movie.Actorslist)
                {
                    var actor = _dbContext.actors.FirstOrDefault(a => a.ActorName == name.ToString()) ?? new Actors { ActorName = name.ToString() };
                    if (!(_dbContext.actors.Contains(actor)))
                    {

                        _dbContext.Add(actor);
                    }

                    act.Add(new ActorsAndMovies { ActorName = name.ToString(), MovieName = movie.MovieName });

                };

                var producer = _dbContext.producers.FirstOrDefault(x => x.ProducerName == movie.ProducerName) ?? new Producers() { ProducerName = movie.ProducerName };

                Movies newmovie = AutoMapper.Mapper.Map<Movies>(movie);               
                    newmovie.ActorsAndMovies = act;
                    newmovie.Producer = producer;
                  _dbContext.movies.Add(newmovie);
                  _dbContext.SaveChanges();

                return true;
            }
            catch(Exception e) {
                return false;
            }
            
           
        }

        public bool Update(MovieView movieView)
        {
            var movie=_dbContext.movies.Find(movieView.MovieName);
            movie.Plot = movieView.Plot;
            movie.Poster = movieView.Poster;
            movie.Producer = _dbContext.producers.Find(movieView.ProducerName) ?? new Producers() { ProducerName = movieView.ProducerName };
            movie.Year = movieView.Year;
            ICollection<ActorsAndMovies> act = new HashSet<ActorsAndMovies>();

            foreach (var name in movieView.Actorslist)
            {
                var actor = _dbContext.actors.FirstOrDefault(a => a.ActorName == name.ToString()) ?? new Actors { ActorName = name.ToString() };
                if (!(_dbContext.actors.Contains(actor)))
                {

                    _dbContext.Add(actor);
                }

                act.Add(new ActorsAndMovies { ActorName = name.ToString(), MovieName = movie.MovieName });

            };
            movie.ActorsAndMovies = act;

            _dbContext.Update(movie);
            _dbContext.SaveChanges();
            return true;
        }

        public bool DeleteMovie(string MovieName)
        {
            try
            {
                var movie = _dbContext.movies.Find(MovieName);
                var posterurl = HttpUtility.UrlDecode(movie.Poster);
                var poster = posterurl.Split("/").Last();
                _blobService.DeleteFromBlob(poster);
                _dbContext.Remove(movie);
                _dbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

  



}
