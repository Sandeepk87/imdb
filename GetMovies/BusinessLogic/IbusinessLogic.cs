﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetMovies.Models;

namespace GetMovies.BusinessLogic
{

    public interface IbusinessLogic
    {
        ICollection<MovieView> GetMovieList();
        bool Save(MovieView movies);

        bool Update(MovieView movie);

        bool CheckMovieExists(MovieView movie);

        bool DeleteMovie(string MovieName);
    }
}
