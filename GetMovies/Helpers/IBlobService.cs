﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GetMovies.Helpers
{
  public  interface IBlobService
    {
         Task<string> UploadToBlob(string filename, byte[] imageBuffer = null, Stream stream = null);
          Task<bool> DeleteFromBlob(string filename);
    }
}
