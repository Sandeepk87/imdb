﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GetMovies.Helpers
{
    public class BlobService:IBlobService
    {

        private CloudStorageAccount cloudStorageAccount;
        private Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient cloudBlobClient;

        private IConfiguration configuration;
        private readonly string accountName;
        private readonly string accountKey;
        private string containerName;

        public BlobService(CloudStorageAccount _cloudStorageAccount, IConfiguration configuration)
        {
            cloudStorageAccount = _cloudStorageAccount;
            //_logger = logger;

            cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

            accountName = configuration.GetSection("BlobSettings:StorageAccountName").Value;
            accountKey = configuration.GetSection("BlobSettings:Key").Value;
            this.configuration = configuration;
            containerName = configuration["BlobSettings:ContainerName"];

        }


        public async Task<string> UploadToBlob(string filename, byte[] imageBuffer = null, Stream stream = null)
        {


            try
            {

                // Create a container 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                await cloudBlobContainer.CreateIfNotExistsAsync();

                // Set the permissions so the blobs are public. 
                BlobContainerPermissions permissions = new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                };
                await cloudBlobContainer.SetPermissionsAsync(permissions);

                // Get a reference to the blob address, then upload the file to the blob.
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(filename);

                if (imageBuffer != null)
                {
                    // OPTION A: use imageBuffer (converted from memory stream)
                    await cloudBlockBlob.UploadFromByteArrayAsync(imageBuffer, 0, imageBuffer.Length);
                    return cloudBlockBlob.Uri.AbsoluteUri;
                }
                else if (stream != null)
                {
                    // OPTION B: pass in memory stream directly
                    await cloudBlockBlob.UploadFromStreamAsync(stream);
                }
                else
                {
                    return "Upload Failed";
                }

                return "Upload Failed";
            }
            catch (StorageException ex)
            {
                return "Upload Failed";
            }



        }


        public async Task <bool> DeleteFromBlob(string filename)
        {
            try
            {
                
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);

                if (await cloudBlobContainer.ExistsAsync())
                {
                    // Get a reference to the blob address, then upload the file to the blob.
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(filename);
                    var test = cloudBlockBlob.Exists();
                    var result = cloudBlockBlob.DeleteIfExistsAsync().GetAwaiter().GetResult();
                    return result;

                 }
                return false;
            }
            catch
            {
                return false;
            }

        }








    }
}
