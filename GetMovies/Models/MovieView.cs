﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GetMovies.Models
{
    public class MovieView
    {

        
        public string MovieName { get; set; }


        
        public string Year { get; set; }

        public string Poster { get; set; }
        public string Plot { get; set; }


        public ICollection<string> Actorslist { get; set; } = new List<string>();

        public string ProducerName { get; set; }
    }
}
