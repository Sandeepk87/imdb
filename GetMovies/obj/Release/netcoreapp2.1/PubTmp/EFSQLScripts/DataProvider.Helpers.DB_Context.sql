﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE TABLE [actors] (
        [Name] nvarchar(450) NOT NULL,
        [Sex] nvarchar(max) NOT NULL,
        [DOB] Date NOT NULL,
        [Bio] nvarchar(max) NULL,
        CONSTRAINT [PK_actors] PRIMARY KEY ([Name])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE TABLE [producers] (
        [Name] nvarchar(450) NOT NULL,
        [Sex] nvarchar(max) NOT NULL,
        [DOB] Date NOT NULL,
        [Bio] nvarchar(max) NULL,
        CONSTRAINT [PK_producers] PRIMARY KEY ([Name])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE TABLE [movies] (
        [Name] nvarchar(100) NOT NULL,
        [Year] int NULL,
        [Poster] nvarchar(max) NULL,
        [Plot] nvarchar(max) NULL,
        [ProducerName] nvarchar(450) NULL,
        CONSTRAINT [PK_movies] PRIMARY KEY ([Name]),
        CONSTRAINT [FK_movies_producers_ProducerName] FOREIGN KEY ([ProducerName]) REFERENCES [producers] ([Name]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE TABLE [ActorsAndMovies] (
        [MovieName] nvarchar(100) NOT NULL,
        [ActorName] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_ActorsAndMovies] PRIMARY KEY ([ActorName], [MovieName]),
        CONSTRAINT [FK_ActorsAndMovies_actors_ActorName] FOREIGN KEY ([ActorName]) REFERENCES [actors] ([Name]) ON DELETE CASCADE,
        CONSTRAINT [FK_ActorsAndMovies_movies_MovieName] FOREIGN KEY ([MovieName]) REFERENCES [movies] ([Name]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE INDEX [IX_ActorsAndMovies_MovieName] ON [ActorsAndMovies] ([MovieName]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    CREATE INDEX [IX_movies_ProducerName] ON [movies] ([ProducerName]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190126092605_initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190126092605_initial', N'2.1.4-rtm-31024');
END;

GO

