﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DataProvider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GetMovies.BusinessLogic;
using DataProvider.Models;
using GetMovies.Models;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Graph;
//using Microsoft.Identity.Client;
using System.Threading;
using System.Net.Http.Headers;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;

namespace GetMovies.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
   
    public class MoviesController : ControllerBase
    {
      
        private IbusinessLogic _businesslogic;
        public MoviesController( IbusinessLogic businessLogic)
        {
          
            _businesslogic = businessLogic;
        }
        

        [Route("GetMovies")]
        [HttpGet]
        public IActionResult GetMovies()
        {
            var newmovies = _businesslogic.GetMovieList();
         
            //string jString=JsonConvert.SerializeObject(newmovies, new JsonSerializerSettings()
            //{
            //    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            //    Formatting = Formatting.Indented
            //});
           string jString=JsonConvert.SerializeObject(newmovies);

            JArray jArray = JArray.Parse(jString);

            return Ok(jArray);
        }
        [Route("AddMovies")]
        [HttpPost]
        public IActionResult AddMovies([FromBody]JObject jObject)
        {
            try
            {
               

                MovieView movieView = jObject.ToObject<MovieView>();
                if(!_businesslogic.CheckMovieExists(movieView))
                {
                   var result= _businesslogic.Save(movieView);
                    return Ok(result);
                }
                return Ok("Movie Exists");
              
               
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException.Message);
            }
          
            
        }
        [Route("EditMovie")]
        [HttpPost]
        public IActionResult EditMovie([FromBody] JObject jObject)
        {
            try
            {
                MovieView movieView = jObject.ToObject<MovieView>();
              var result=  _businesslogic.Update(movieView);
                return Ok(result);
            }
            catch
            {
                return BadRequest();
            }

        }
        [Route("DeleteMovie/{MovieName}")]
        [HttpDelete("{MovieName}")]
        public IActionResult DeleteMovie(string MovieName)
        {
            try {
              var result=  _businesslogic.DeleteMovie(MovieName);
                    return Ok(result);
            }
            catch { return BadRequest(); }

        }

    }
}